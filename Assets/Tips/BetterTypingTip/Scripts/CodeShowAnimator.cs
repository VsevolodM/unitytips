using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Tips.BetterTypingTip.Scripts
{
    public class CodeShowAnimator : MonoBehaviour
    {
        [SerializeField] private float animationDuration = 1f;
        [SerializeField] private Transform goodCode;
        [SerializeField] private Transform badCode;
        [SerializeField] private TextMeshProUGUI text;

        private void Awake()
        {
            SetScaleToZero();
        }

        private void SetScaleToZero()
        {
            goodCode.localScale = new Vector3(1f, 0f, 1f);
            badCode.localScale = new Vector3(1f, 0f, 1f);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                SetScaleToZero();
                text.text = "<align=center><b>Follow for more!</b></align>";
                goodCode.DOScale(Vector3.one, animationDuration);
                badCode.DOScale(Vector3.one, animationDuration);
            }
        }
    }
}
