using System.Collections;
using TMPro;
using UnityEngine;

namespace Tips.BetterTypingTip.Scripts
{
    public class MaxCharactersTextPrinter : MonoBehaviour
    {
        [SerializeField] private TMP_Text tmpText;
        
        private IEnumerator TypeText(string input, float interval)
        {
            var typeCharacterDelay = new WaitForSeconds(interval);
            tmpText.text = input;
            
            for (var i = 0; i < input.Length; i++)
            {
                tmpText.maxVisibleCharacters = i + 1;
                yield return typeCharacterDelay;
            }
        }
        
        public void TypeTextOvertime(string inputText, float typeSpeed)
        {
            StartCoroutine(TypeText(inputText, typeSpeed));
        }
    }
}
