using UnityEngine;
using UnityEngine.UI;

namespace Tips.BetterTypingTip.Scripts
{
    public class MaxCharactersTextPrinterButton : MonoBehaviour
    {
        [SerializeField] private MaxCharactersTextPrinter stringConcatTextPrinter;
        [SerializeField] private Button button;

        private void Awake()
        {
            button.onClick.AddListener(CallText);
        }

        private void CallText()
        {
            stringConcatTextPrinter.TypeTextOvertime("Showing a text character by character via setting a new string to the text component, will cause a jump on line overflow and won't handle <i>tags</i> right. Instead, set the text right away and change \"maxVisibleCharacters\" overtime to avoid this.", 0.045f);
        }
    }
}
