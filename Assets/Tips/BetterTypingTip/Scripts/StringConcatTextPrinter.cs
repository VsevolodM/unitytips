using System.Collections;
using TMPro;
using UnityEngine;

namespace Tips.BetterTypingTip.Scripts
{
    public class StringConcatTextPrinter : MonoBehaviour
    {
        [SerializeField] private TMP_Text tmpText;

        private IEnumerator TypeText(string input, float interval)
        {
            var typeCharacterDelay = new WaitForSeconds(interval);
            
            for (var i = 0; i < input.Length; i++)
            {
                tmpText.text = input.Substring(0, i + 1);
                yield return typeCharacterDelay;
            }
        }
        
        
        public void TypeTextOvertime(string inputText, float typeSpeed)
        {
            tmpText.text = string.Empty;
            StartCoroutine(TypeText(inputText, typeSpeed));
        }

    }
}
