using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Tips.DisableEventSystem.Scripts
{
    public class LoginPanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text logText;
        [SerializeField] private Button loginButton;
        [SerializeField] private GameObject loadingIcon;

        private EventSystem _currentEventSystem;

        private void Awake()
        {
            loadingIcon.SetActive(false);
            logText.text = string.Empty;
        }

        private void OnLoginButtonClicked()
        {
            loadingIcon.SetActive(true);
            _currentEventSystem = EventSystem.current;
            _currentEventSystem.enabled = false;
            StartCoroutine(WaitForLoading());
            logText.text += "Sending login data to the server...\n";
        }

        private IEnumerator WaitForLoading()
        {
            yield return new WaitForSeconds(6f);
            _currentEventSystem.enabled = true;
            loadingIcon.SetActive(false);
        }

        private void OnEnable()
        {
            loginButton.onClick.AddListener(OnLoginButtonClicked);
        }

        private void OnDisable()
        {
            loginButton.onClick.RemoveListener(OnLoginButtonClicked);
        }
    }
}
