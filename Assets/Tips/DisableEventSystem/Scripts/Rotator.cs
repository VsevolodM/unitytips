﻿using UnityEngine;

namespace Tips.DisableEventSystem.Scripts
{
    public class Rotator : MonoBehaviour
    {
        [SerializeField] float rotationSpeed = 200;

        private void Update()
        {
            transform.Rotate( Vector3.back * Time.deltaTime * rotationSpeed, Space.Self);
        }
    }
}
