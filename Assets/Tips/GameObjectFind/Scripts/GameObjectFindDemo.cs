using UnityEngine;

namespace Tips.GameObjectFind.Scripts
{
    public class GameObjectFindDemo : MonoBehaviour
    {
        private void Awake()
         {
             //unpredictable if scene has more than one object
             var innerObject1 = GameObject.Find("InnerObject");
             
             Debug.LogWarning(nameof(innerObject1), innerObject1);
             
             //looks for a InnerObject that is a child of Parent1
             var parentedObject1 = GameObject.Find("Parent1/InnerObject");
             
             Debug.LogWarning(nameof(parentedObject1), parentedObject1);
             
             //looks for a InnerObject that is a child of Parent2
             var parentedObject2 = GameObject.Find("Parent2/InnerObject");
             
             Debug.LogWarning(nameof(parentedObject2), parentedObject2);
         }
    }
}
