using UnityEngine;
using UnityEngine.UI;

namespace Tips.ResetMethod.Scripts
{
    public class ResetMethodExample : MonoBehaviour
    {
        [SerializeField] private Selectable[] interactableObjects;

        /// <summary>
        /// Is called upon adding this component to a gameObject or pressing Reset from the component context menu 
        /// </summary>
        private void Reset()
        {
            //To make sure it is being called when we expect it to
            Debug.LogWarning("Reset!");
            
            //Fill the interactableObjects array with Selectable components found among children of the current transform
            interactableObjects = transform.GetComponentsInChildren<Selectable>();

            //loop through all the found Selectable components and set their interactable bool to true
            for (int i = 0; i < interactableObjects.Length; i++)
            {
                interactableObjects[i].interactable = true;
            }

            //Set the current gameObject's name to be the same as the name of the current class
            gameObject.name = GetType().Name;
        }
    }
}
