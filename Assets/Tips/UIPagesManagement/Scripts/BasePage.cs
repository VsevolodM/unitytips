using UnityEngine;
using UnityEngine.UI;

namespace Tips.UIPagesManagement.Scripts
{
    [RequireComponent(typeof(Canvas), typeof(GraphicRaycaster), typeof(CanvasScaler))]
    public abstract class BasePage : MonoBehaviour
    {
        public bool IsShown { get; private set; }                                                            //Can be used by other scripts to determine if page is visible
        private Canvas canvas;
        
        private Canvas Canvas                                                                                //"Lazy initialization". The canvas field will only get initialized once Canvas property is called
        {
            get
            {
                if(canvas == null) canvas = GetComponent<Canvas>();
                return canvas;
            }
        }
        
        private GraphicRaycaster graphicRaycaster;
        
        private GraphicRaycaster GraphicRaycaster                                                            //"Lazy initialization". The graphicRaycaster field will only get initialized once GraphicRaycaster property is called
        {
            get
            {
                if(graphicRaycaster == null) graphicRaycaster = GetComponent<GraphicRaycaster>();
                return graphicRaycaster;
            }
        }

        public void Show(int canvasSortingOrder)
        {
            IsShown = true;                            //Can be used by other scripts to determine if page is visible
            Canvas.enabled = true;                     //Enabling Canvas to make UI render
            Canvas.sortingOrder = canvasSortingOrder;  //Setting sortingOrder to the Canvas (the higher the number, the closer to the camera UI will be rendered)
            GraphicRaycaster.enabled = true;           //Enabling GraphicRaycaster so that Unity can track buttons clicks etc
            OnShown();                                 //Calling OnShown method that should contain whatever we want to do after UI is shown
        }

        protected virtual void OnShown() {}            //Overload this method in derived class and add logic you want to be executed when UI is shown
        
        public void Hide()
        {
            IsShown = false;                           //Can be used by other scripts to determine if page is visible
            Canvas.enabled = false;                    //Disabling Canvas will make Unity stop rendering all the UI elements inside (including children)
            GraphicRaycaster.enabled = false;          //Disabling GraphicRaycaster will stop tracking buttons clicks etc and will save you from possible interaction with invisible UI
            OnHidden();                                //Calling OnHidden method that should contain whatever we want to do after UI is hidden
        }

        protected virtual void OnHidden() {}           //Overload this method in derived class and add logic you want to be executed when UI is hidden
    }
}
