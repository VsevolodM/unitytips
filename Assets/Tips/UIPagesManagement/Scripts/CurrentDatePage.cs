using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Tips.UIPagesManagement.Scripts
{
    public class CurrentDatePage : BasePage
    {
        private readonly WaitForSeconds _dateRefreshInterval = new WaitForSeconds(1f);
        
        [SerializeField] private Button backButton;
        [SerializeField] private TMP_Text dateTimeText;
        private Coroutine _dateRefreshingCoroutine;
        
        protected override void OnShown()                                                     //This method is called when UI is shown
        {
            base.OnShown();                                                                   //Execute whatever logic this method has in base class (BasePage)
            backButton.onClick.AddListener(PagesController.Instance.Back);                    //Make it so that clicking backButton with call PagesController.Instance.Back method
            _dateRefreshingCoroutine = StartCoroutine(DateRefreshing());                      //Start coroutine that will be refreshing date text once per second
        }

        protected override void OnHidden()                                                    //This method is called when UI is hidden                        
        {
            base.OnHidden();                                                                  //Execute whatever logic this method has in base class (BasePage)
            backButton.onClick.RemoveListener(PagesController.Instance.Back);                 //Make it so that clicking backButton with no longer be calling PagesController.Instance.Back method            
            if(_dateRefreshingCoroutine != null) StopCoroutine(_dateRefreshingCoroutine);     //If the date text refreshing coroutine has started - stop it
        }

        private IEnumerator DateRefreshing()                                                  //This method is a coroutine that will be refreshing date text once per second
        {
            while(IsShown)                                                                    //Loop will be executing until UI is not hidden
            {
                dateTimeText.text = DateTime.UtcNow.ToString("HH:mm:ss");                     //Set the text value to show current UTC time
                yield return _dateRefreshInterval;                                            //Wait for 1 second before executing next loop iteration
            }
        }
    }
}
