using UnityEngine;

namespace Tips.UIPagesManagement.Scripts
{
    //The only purpose of this class is to start the app flow
    public class EntryPoint : MonoBehaviour
    {
        //Start is a default method available in all MonoBehaviour classes that is get called only once right after Awake, OnEnable and Reset methods are done
        private void Start()                                        
        {
            //Open the main menu page
            PagesController.Instance.OpenPage<MainMenuPage>();    
        }
    }
}