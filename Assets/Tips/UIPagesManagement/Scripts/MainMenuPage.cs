using UnityEngine;
using UnityEngine.UI;

namespace Tips.UIPagesManagement.Scripts
{
    public class MainMenuPage : BasePage
    {
        [SerializeField] private Button openCurrentDatePageButton;

        private void OnOpenTimerPageButtonClicked()
        {
            PagesController.Instance.OpenPage<CurrentDatePage>();
        }
        
        protected override void OnShown()                                                              //This method is called when UI is shown
        {
            base.OnShown();                                                                            //Execute whatever logic this method has in base class (BasePage)
            openCurrentDatePageButton.onClick.AddListener(OnOpenTimerPageButtonClicked);               //Make it so that clicking backButton with call OnOpenTimerPageButtonClicked method
        }

        protected override void OnHidden()                                                             //This method is called when UI is hidden   
        {
            base.OnHidden();                                                                           //Execute whatever logic this method has in base class (BasePage)
            openCurrentDatePageButton.onClick.RemoveListener(OnOpenTimerPageButtonClicked);            //Make it so that clicking backButton with no longer be calling OnOpenTimerPageButtonClicked method
        }
    }
}
