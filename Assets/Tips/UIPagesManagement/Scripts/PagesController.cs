using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tips.UIPagesManagement.Scripts
{
    public class PagesController : MonoBehaviour
    {
        [SerializeField] private BasePage[] allPages;                            //This array contains all the pages in the app. They should be inside from the object Inspector
        private readonly List<BasePage> _openedPages = new List<BasePage>();     //This list contains all the pages that are currently shown (IsShown bool is set to true)

        private void Awake()
        {
            for (int i = 0; i < allPages.Length; i++)                            //Let's hide all the pages on the app start
            {
                allPages[i].Hide();
            }
        }

        public void OpenPage<T>()                                                //This method opens up a new page
        {
            for (int i = 0; i < allPages.Length; i++)                            //Let's loop though all the available pages
            {
                if (allPages[i] is T)                                            //If current page is the one we are looking for
                {
                    HideCurrentPage(false);                                      //Hide current page but don't remove it from _openedPages list (because we will need to go back to it later)
                    allPages[i].Show(_openedPages.Count + 1);                    //Show the page we found and pass amount of items in _openedPages list to set as a SortingOrder value for the Canvas
                    _openedPages.Add(allPages[i]);                               //Add the page to the _openedPages list, so we can go back to it later if needed
                    return;                                                      //If we already found the page we wanted to open and opened it, we can stop executing this method
                }
            }
        }

        public void Back()                                                      //This method goes back to the previous page whatever that page is
        {
            HideCurrentPage(true);                                              //Hide current page but and remove it from _openedPages list
            ShowLastOpenedPage();                                               //Show new current page (which is one page before the one we have hidden one line above)
        }

        private void ShowLastOpenedPage()                                       //This method shows the last page in the _openedPages list
        {
            if (_openedPages.Count == 0) return;                                //If the _openedPages is empty - don't do anything
            
            var lastOpenedPage = _openedPages.Last();                           //Get the last page from the _openedPages list
            lastOpenedPage.Show(_openedPages.Count);                            //Show the page and pass amount of items in _openedPages list to set as a SortingOrder value for the Canvas 
        }
        
        private void HideCurrentPage(bool removeFromHistory)                    //This method hides the last page in the _openedPages list
        {
            if (_openedPages.Count == 0) return;                                //If the _openedPages is empty - don't do anything

            var lastOpenedPage = _openedPages.Last();                           //Get the last page from the _openedPages list
            lastOpenedPage.Hide();                                              //Hide the page
            if(removeFromHistory) _openedPages.Remove(lastOpenedPage);          //If removeFromHistory is true, remove the page from the _openedPages list
        }


        //We only need one PagesController per app, so this class is made to be a Singleton
        private static PagesController _instance;

        //We can use this class by typing PagesController.Instance
        public static PagesController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<PagesController>();
                return _instance;
            }
        }
    }
}
